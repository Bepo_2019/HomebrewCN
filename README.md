# 安装 Git

Git 原下载地址（速度慢）： https://git-scm.com/download/mac  
Git 国内镜像地址（速度快）： https://www.newbe.pro/Mirrors/Mirrors-Git-For-MacOS/

# HomebrewCN
Homebrew 国内自动安装脚本 ，修改原脚本中的 clone 操作为“浅拷贝”（--depth 1), 克隆速度快了十几倍。

/bin/zsh -c "$(curl -fsSL https://gitee.com/jyotish/HomebrewCN/raw/master/Homebrew.sh)"

